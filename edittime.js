function GetPluginSettings() {
    return {
        "name": "HTTPS File Downloader",
        "id": "file_downloader",
        "version": "1.0",
        "description": "Download files using NW.js",
        "author": "Tazik05 (stas's ports)",
        "help url": "https://gitlab.com/TheStazik/https-file-downloader",
        "category": "General",
        "type": "object",
        "rotatable": false,
        "dependency": "",
        "flags": pf_singleglobal
    };
}

// Conditions
AddStringParam("Tag", "A tag to identify the download.");
AddCondition(0, cf_trigger, "On download started", "Download", "On download started for tag {0}", "Triggered when a download starts.", "OnDownloadStarted");

AddStringParam("Tag", "A tag to identify the download.");
AddCondition(1, cf_trigger, "On download completed", "Download", "On download completed for tag {0}", "Triggered when a download completes.", "OnDownloadCompleted");

AddStringParam("Tag", "A tag to identify the download.");
AddCondition(2, cf_trigger, "On download failed", "Download", "On download failed for tag {0}", "Triggered when a download fails.", "OnDownloadFailed");

AddStringParam("Tag", "A tag to identify the download.");
AddCondition(3, 0, "Is download in progress", "Download", "Is download in progress for tag {0}", "Check if a download is in progress for a specific tag.", "IsDownloadInProgress");

// Actions
AddStringParam("URL", "The URL of the file to download.");
AddStringParam("Save path", "The path to save the downloaded file.");
AddStringParam("Tag", "A tag to identify the download.");
AddAction(0, af_none, "Download file", "Download", "Download file from <b>{0}</b> to <b>{1}</b> with tag <b>{2}</b>", "Download a file from a URL.", "DownloadFile");

// Expressions
AddExpression(0, ef_return_number, "Get download speed", "Download", "DownloadSpeed", "Get the download speed in bytes per second for the current tag.");
AddExpression(1, ef_return_number, "Get download progress", "Download", "DownloadProgress", "Get the download progress as a percentage for the current tag.");

ACESDone();

var property_list = [];

function CreateIDEObjectType() {
    return new IDEObjectType();
}

function IDEObjectType() {}

IDEObjectType.prototype.CreateInstance = function(instance) {
    return new IDEInstance(instance);
}

function IDEInstance(instance, type) {
    this.instance = instance;
    this.type = type;
    this.properties = {};
    for (var i = 0; i < property_list.length; i++)
        this.properties[property_list[i].name] = property_list[i].initial_value;
}

IDEInstance.prototype.OnInserted = function() {}
IDEInstance.prototype.OnDoubleClicked = function() {}
IDEInstance.prototype.OnPropertyChanged = function(property_name) {}
IDEInstance.prototype.OnRendererInit = function(renderer) {}
IDEInstance.prototype.Draw = function(renderer) {}
IDEInstance.prototype.OnRendererReleased = function(renderer) {}

