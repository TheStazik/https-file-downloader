cr.plugins_.file_downloader = function(runtime) {
    this.runtime = runtime;
};

(function() {
    var pluginProto = cr.plugins_.file_downloader.prototype;

    pluginProto.Type = function(plugin) {
        this.plugin = plugin;
        this.runtime = plugin.runtime;
    };

    var typeProto = pluginProto.Type.prototype;

    typeProto.onCreate = function() {};

    pluginProto.Instance = function(type) {
        this.type = type;
        this.runtime = type.runtime;
        this.downloads = {};
        this.currentTag = "";
    };

    var instanceProto = pluginProto.Instance.prototype;

    instanceProto.onCreate = function() {
        this.downloads = {};
    };

    instanceProto.DownloadFile = function(url, savePath, tag) {
        const https = require('https');
        const fs = require('fs');

        const downloadInfo = {
            url: url,
            savePath: savePath,
            tag: tag,
            startTime: Date.now(),
            downloadedBytes: 0,
            contentLength: 0,
            inProgress: true
        };

        this.downloads[tag] = downloadInfo;

        const file = fs.createWriteStream(savePath);
        const request = https.get(url, (response) => {
            if (response.statusCode === 200) {
                downloadInfo.contentLength = parseInt(response.headers['content-length'], 10);
                response.pipe(file);
                response.on('data', (chunk) => {
                    downloadInfo.downloadedBytes += chunk.length;
                });

                file.on('finish', () => {
                    file.close(() => {
                        downloadInfo.inProgress = false;
                        this.currentTag = tag;
                        this.runtime.trigger(cr.plugins_.file_downloader.prototype.cnds.OnDownloadCompleted, this);
                    });
                });

                this.currentTag = tag;
                this.runtime.trigger(cr.plugins_.file_downloader.prototype.cnds.OnDownloadStarted, this);
            } else {
                fs.unlink(savePath, () => {});
                downloadInfo.inProgress = false;
                this.currentTag = tag;
                this.runtime.trigger(cr.plugins_.file_downloader.prototype.cnds.OnDownloadFailed, this);
            }
        }).on('error', (err) => {
            fs.unlink(savePath, () => {});
            downloadInfo.inProgress = false;
            this.currentTag = tag;
            this.runtime.trigger(cr.plugins_.file_downloader.prototype.cnds.OnDownloadFailed, this);
        });
    };

    instanceProto.GetDownloadSpeed = function(tag) {
        const downloadInfo = this.downloads[tag];
        if (downloadInfo) {
            const timeElapsed = (Date.now() - downloadInfo.startTime) / 1000;
            return downloadInfo.downloadedBytes / timeElapsed;
        }
        return 0;
    };

    instanceProto.GetDownloadProgress = function(tag) {
        const downloadInfo = this.downloads[tag];
        if (downloadInfo && downloadInfo.contentLength > 0) {
            return (downloadInfo.downloadedBytes / downloadInfo.contentLength) * 100;
        }
        return 0;
    };

    instanceProto.IsDownloadInProgress = function(tag) {
        const downloadInfo = this.downloads[tag];
        return downloadInfo && downloadInfo.inProgress;
    };

    // Conditions
    pluginProto.cnds = {
        OnDownloadStarted: function(tag) {
            this.currentTag = tag;
            return true;
        },
        OnDownloadCompleted: function(tag) {
            this.currentTag = tag;
            return true;
        },
        OnDownloadFailed: function(tag) {
            this.currentTag = tag;
            return true;
        },
        IsDownloadInProgress: function(tag) {
            return this.IsDownloadInProgress(tag);
        }
    };

    // Actions
    pluginProto.acts = {
        DownloadFile: function(url, savePath, tag) {
            this.DownloadFile(url, savePath, tag);
        }
    };

    // Expressions
    pluginProto.exps = {
        DownloadSpeed: function(ret) {
            ret.set_float(this.GetDownloadSpeed(this.currentTag));
        },
        DownloadProgress: function(ret) {
            ret.set_float(this.GetDownloadProgress(this.currentTag));
        }
    };
}());

